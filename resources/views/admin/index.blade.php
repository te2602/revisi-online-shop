@extends('template_backend.home')

@section('content')
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="welcome-wrapper shadow-reset res-mg-t mg-b-30">
      <h3>Halaman Admin</h3>
      <br>

      <ul class="list-group">
        <li class="list-group-item active" aria-current="true">Kelompok 1</li>
        <li class="list-group-item">Guntur Oktavianto Nugroho (@gunturoktavianto)</li>
        <li class="list-group-item">Harry Helvizar (@harryhelvizar)</li>
        <li class="list-group-item">Muhammad Ramdhani (@maSTER_jowo)</li>
        <li class="list-group-item active" aria-current="true">Tema Project : online shop cars</li>

      </ul>
    </div>
  </div>
@endsection
