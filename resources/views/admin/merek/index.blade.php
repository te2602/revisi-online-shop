@extends('template_backend.home')
@section('halaman', 'List Merek')

@push('script')
    <script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
      $(function () {
        $("#example1").DataTable();
      });
    </script>
@endpush

@push('style')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush

@section('content')
  

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="welcome-wrapper shadow-reset res-mg-t mg-b-30">
      
        <a href="{{ route('merek.create') }}" class="btn btn-primary btn-sm">Tambah Merek</a>
        <a href="/test-excel" class="btn btn-success btn-sm">Export Excel</a><br><br>
      
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Merek</th>
            <th>Action</th>
          </tr>
        </thead>
        @foreach ($merek as $result => $d)
          <tr>
            <td>{{ $result + $merek->firstitem() }}</td>
            <td>{{ $d->name }}</td>
            <td>
              <form action="{{ route('merek.destroy', $d->id) }}" method="post">
                @csrf
                @method('delete')
                <a href="{{ route('merek.edit', $d->id) }}" class="btn btn-success btn-sm">Edit</a>
                <button type="submit" class="btn btn-danger btn-sm" name="button">Delete</button>
              </form>
            </td>
          </tr>
        @endforeach
      </table>
      {{ $merek->links() }}
    </div>
  </div>
@endsection
