# Kelompok 1

-   Guntur Oktavianto Nugroho (@gunturoktavianto)
-   Harry Helvizar (@harryhelvizar)
-   Muhammad Ramdhani (@maSTER_jowo)

Tema Project : online shop cars

## Require

-   table "favorite" terdapat data : bigIncrements('id'),integer('user_id'),integer('mobil_id'),timestamps()

-   table "order" terdapat data :bigIncrements('id'),integer('quantity'),integer('total'),string('payment_status'),unsignedBigInteger('user_id'),unsignedBigInteger('mobil_id'),timestamps()

-   table "bukti" terdapat data : bigIncrements('id'),string('foto'),string('nama_bank'),string('nama_pengirim'),unsignedBigInteger('order_id')

-   table "merek" terdapat data : bigIncrements('id'),string('name'),timestamps()

-   table "mobil" terdapat data : bigIncrements('id'),integer('merek_id'),string('type'),integer('price'),string('gambar'),timestamps(),bigIncrements('merek_id')

-   table "user" terdapat data : bigIncrements('id'), string('name'),enum('role', ['admin', 'user']),string('email'),timestamp('email_verified_at'),string('pekerjaan'),date('tanggal_lahir'),string('telepon'),string('address'),string('kelurahan'),string('kecamatan'),string('kabupaten'),string('provinsi'),string('gambar'),string('password'),rememberToken(),timestamps(),

-   table "password" terdapat data : string('email'),string('token'),timestamp('created_at'),integer('user_id')

## nb : Untuk ERD Diagram bisa dilihat sini

![](https://drive.google.com/uc?export=view&id=1xSCc9ZC3_WOa8RDE_CDENqVsBc-ls7fC)

-   [img](https://drive.google.com/file/d/1xSCc9ZC3_WOa8RDE_CDENqVsBc-ls7fC/view?usp=sharing)

## Link Video

-   [youtube](https://youtu.be/GxnmH02XjmY)

## Link Deploy

-   [Heroku/Hosting](http://onlineshop.projectahir.sanbercodeapp.com/login)
    akun login admin : dani@gmail.com (pass: 123456789)
